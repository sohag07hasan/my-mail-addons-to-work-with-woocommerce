<style>
	
	table.newsletter_table{
		padding: 10px;
		margin-top: 20px;
	}
	
	table.newsletter_table td{
		padding-top: 5px;
	}
	
	.status_text{
		font-weight: bold;
		font-size: 17px;
	}	
	
</style>
<div style="clear: both"></div>
<table class="newsletter_table">
	<tr>
		<td>
			<input id="mymail_newsletter" type="checkbox" name="mymail_newsletter" value="accepted" /> 
		</td>
		<td>			
			<span class="status_text"> Receive great offers and updates on my favourite tasty temptations </span>
		</td>
	</tr>
</table>