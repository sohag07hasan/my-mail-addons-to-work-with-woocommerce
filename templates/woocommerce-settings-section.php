<h4>Woocomerce Integration</h4>
<p>Please select the lists. During checkout, customers will see the options for subscription</p>

<?php
$chosen_lists = mymail_option('woocommerce');

$chosen_lists = is_array($chosen_lists) ? $chosen_lists : array();

$lists = get_terms('newsletter_lists', array('hide_empty' => false, 'fields' => 'all'));
if($lists) :
?>
	
<table class="form-table">
	<?php foreach($lists as $list):  ?>
	
		<?php $checked = in_array($list->slug, $chosen_lists) ? 1 : 0; ?>
		
		<tr valign="top">
			<td>
				<input <?php checked(1, $checked); ?> type="checkbox" name="mymail_options[woocommerce][]" id="<?php echo $list->slug; ?>" value="<?php echo $list->slug; ?>" /> &nbsp; &nbsp;
				<label for="<?php echo $list->slug; ?>"> <?php echo $list->name; ?> </label>
			</td>					
		</tr>
		
	<?php endforeach; ?>
</table>
<?php return; ?>
<?php endif;?>

<p>The Subscirbers list is empty. Please add some and come back here.</p>