<?php
/**
 * Plugin Name: My Mail addons to work with woocommerce
 * Description: ..........
 * Author: Mahibul Hasan Sohag
 * Author URI: http://sohag07hasan.elance.com
 * */

class MyMailAddonWoocommerce{
	
	const Taxonomy = "newsletter_lists";
		
	static function init(){
		//add an extra tab
		add_filter('mymail_setting_sections', array(get_class(), 'add_woocommerce_setting_sections'), 100, 1);
		
		//populate extra tab content		
		add_action('mymail_section_tab_woocommerce', array(get_class(), 'populate_woocommerce_setcion'));
		
		//show subscription option at checkout
		//add_action('woocommerce_checkout_after_customer_details', array(get_class(), 'show_subscription_checkbox'));
		
		//Add the subscription option after billing address
		add_action('woocommerce_review_order_before_submit', array(get_class(), 'show_subscription_checkbox'), 10, 1);
		
		
		//after processing checkout newletter action
		add_action('woocommerce_checkout_order_processed', array(get_class(), 'subscribe_action'), 10, 2);
	}
	
	//add woocommerce tab
	static function add_woocommerce_setting_sections($options){
		$options['woocommerce'] = __("Woocommerce", "mymail");
		return $options;
	}
	
	
	//populate extra tab content		
	static function populate_woocommerce_setcion(){
		$lists = get_terms(self::Taxonomy, array('hide_empty' => false, 'fields' => 'all'));
		include dirname(__FILE__) . '/' . 'templates/woocommerce-settings-section.php';
	}

	
	//showing subscription option @ checkout
	static function show_subscription_checkbox(){
		include dirname(__FILE__) . '/' . 'templates/woocommerce/subscription-form.php';
	}
	
	
	static function subscribe_action($order_id, $posted){
	
		
		if($_POST['mymail_newsletter'] == 'accepted'):

			global $mymail_subscriber;			
			
			$userdata = array();				
			$email = $posted['billing_email'];	
			$chosen_lists = mymail_option('woocommerce');
						
			
			$userdata['firstname'] = $posted['billing_first_name'];
			$userdata['lastname'] = $posted['billing_last_name'];
			
			
			return $mymail_subscriber->subscribe($email, $userdata, $chosen_lists, false );					
			
		endif;
				
	}
	
}


MyMailAddonWoocommerce::init();
 
?>
